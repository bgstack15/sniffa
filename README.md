sniffa
======

#### This is the bgstack15 fork of sniffa!

[sniffa](https://github.com/danielmitterdorfer/sniffa) is a small utility that allows you to watch Discuss forums for keywords.

Every time it is invoked, it checks for new posts matching the keywords and creates a notification in the Discord room for the webhook provided in the config file.

# Requirements

* Python 3
* certifi: Install with `yum install python3-certifi` or `pip3 install --user certifi`
* `pip3 install --user discord`

# Installation

Ensure that all prerequisites are installed, then copy `sniffa.py` to any directory, e.g. `~/bin` and run `chmod u+x sniffa.py`.

# Usage

sniffa can be used to query multiple Discuss forums. The keywords and the ids of all already seen posts are maintained in a file `~/.sniffa/watch-$(FORUM_NAME).ini`, where `$(FORUM_NAME)` is a name that you can choose to identify this forum.

If you have a file named `~/.sniffa/watches-aoe2.ini`, run this command:

    ./sniffa.py aoe2

## Example

Consider you want to watch for the keywords "Rally" and "JMeter" in the Elastic Discuss forum at https://discuss.elastic.co.

1. Create `~/.sniffa/watch-elastic.ini`
2. Add the following lines:

```
[sniffa.domain]
url = https://discuss.elastic.co

[Rally]

[JMeter]
```

Now invoke sniffa: ``./sniffa.py elastic``. It will load the watches file for the forum named "elastic", check for new posts (which will be a lot at the first time) and send an alert to your Discord webhook.

See the included `watch-example.ini`.

# License

'sniffa' is distributed under the terms of the [Apache Software Foundation license, version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).
